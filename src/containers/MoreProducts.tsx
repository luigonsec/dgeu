import { connect } from 'react-redux';
import MoreProducts from '../components/Sections/MoreProducts';
import IGlobalState from 'src/state/globalState';
import { Dispatch } from 'redux';
import { MoreProductsActions } from '../actions/MoreProductsActions';
import IProduct from 'src/interfaces/IProduct';

const mapStateToProps = (state: IGlobalState) => {
    return (
        {
            order: state.order,            
        }
    )
}

const mapDispatchToProps = (dispatch: Dispatch) => ({
    dispatchOrder: (products: IProduct[]) => {
        dispatch({type: MoreProductsActions.UPDATE_ORDER, payload: products});
    },

})
export default connect(mapStateToProps, mapDispatchToProps)(MoreProducts);