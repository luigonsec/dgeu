import { connect } from 'react-redux';
import Inventory from '../components/Sections/Inventory';
import IGlobalState from 'src/state/globalState';
import { Dispatch } from 'redux';
import { InventoryActions } from '../actions/InventoryActions';
import IProduct from 'src/interfaces/IProduct';

const mapStateToProps = (state: IGlobalState) => {
    return (
        {
            inventory: state.inventory,            
        }
    )
}

const mapDispatchToProps = (dispatch: Dispatch) => ({
    dispatchOrder: (inventory: IProduct[]) => {
        dispatch({type: InventoryActions.UPDATE_INVENTORY, payload: inventory});
    },

})
export default connect(mapStateToProps, mapDispatchToProps)(Inventory);