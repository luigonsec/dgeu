import Header from '../components/Header';
import { connect } from 'react-redux';
import IGlobalState from '../state/globalState';
import { Dispatch } from 'redux';
import { HeaderActions } from '../actions/HeaderActions';

const mapStateToProps = (state: IGlobalState) => ({
    active: state.active,
    collapsed: state.collapsed,
    order: state.order,
    total: state.total,
})

const mapDispatchToProps = (dispatch: Dispatch) => ({
    dispatchActive: (active: string) => {
        dispatch({type: HeaderActions.ACTIVE, payload: active});
    },
    dispatchCollapse: (collapsed: boolean) => {
        dispatch({type: HeaderActions.COLLAPSE, payload: collapsed});
    },
    dispatchTotal: (total: number) => {
        dispatch({type: HeaderActions.UPDATE_TOTAL, payload: total});
    },

})

export default connect(mapStateToProps, mapDispatchToProps)(Header);
