import { connect } from 'react-redux';
import Product from '../components/Sections/partials/Product';
import IGlobalState from 'src/state/globalState';
import { Dispatch } from 'redux';
import { MoreProductsActions } from '../actions/MoreProductsActions';
import IProduct from 'src/interfaces/IProduct';
import { InventoryActions } from 'src/actions/InventoryActions';

const mapStateToProps = (state: IGlobalState) => {
    return (
        {   
            inventory: state.inventory,
            order: state.order,            
        }
    )
}

const mapDispatchToProps = (dispatch: Dispatch) => ({
    dispatchInventory: (products: IProduct[]) => {
        dispatch({type: InventoryActions.UPDATE_INVENTORY, payload: products});
    },
    dispatchOrder: (products: IProduct[]) => {
        dispatch({type: MoreProductsActions.UPDATE_ORDER, payload: products});
    },
})
export default connect(mapStateToProps, mapDispatchToProps)(Product);