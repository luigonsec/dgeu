import { connect } from 'react-redux';
import Orders from '../components/Sections/Orders';
import IGlobalState from 'src/state/globalState';
import { Dispatch } from 'redux';
import { MainActions } from '../actions/MainActions';

const mapStateToProps = (state: IGlobalState) => {
    return (
        {
            order: state.order,            
            total: state.total,
        }
    )
}

const mapDispatchToProps = (dispatch: Dispatch) => ({
    dispatchTotal: (total: number) => {
        dispatch({type: MainActions.UPDATE_TOTAL, payload: total});
    },

})

export default connect(mapStateToProps, mapDispatchToProps)(Orders);