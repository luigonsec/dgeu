import { connect } from 'react-redux';
import Main from '../components/Main';
import IGlobalState from 'src/state/globalState';
import { Dispatch } from 'redux';
import { MainActions } from '../actions/MainActions';

const mapStateToProps = (state: IGlobalState) => {
    return (
        {
            active: state.active,
            collapsed: state.collapsed,
            total: state.total
        }
    )
}

const mapDispatchToProps = (dispatch: Dispatch) => ({
    dispatchTotal: (total: number) => {
        dispatch({type: MainActions.UPDATE_TOTAL, payload: total});
    },

})

export default connect(mapStateToProps, mapDispatchToProps)(Main);