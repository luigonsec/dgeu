export default interface IProduct {
    id: number,
    name: string,
    amount?: number;
    measurement: string,
    price: number,
    img: string,
    info?: any
}