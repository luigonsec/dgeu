import * as React from 'react';
import './assets/css/main.css';
import Header from './containers/Header';
import Main from './containers/Main';
import { Provider } from 'react-redux';
import { store } from './state/store';

class App extends React.Component {
  public render() {
    return (
      <Provider store={store}>
        <div className="App container-fluid">
          <Header />
          <Main />
        </div>
      </Provider>
    );
  }
}

export default App;
