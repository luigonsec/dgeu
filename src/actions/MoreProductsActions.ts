import { Action } from 'redux';
import IProduct from 'src/interfaces/IProduct';

export enum MoreProductsActions {
    UPDATE_ORDER = "UPDATE_ORDER"
}

export interface IUpdateOrderAction extends Action {
    payload: IProduct[];
}
