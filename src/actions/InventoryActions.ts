import { Action } from 'redux';
import IProduct from 'src/interfaces/IProduct';

export enum InventoryActions {
    UPDATE_INVENTORY = "UPDATE_INVENTORY"
}

export interface IUpdateInventoryAction extends Action {
    payload: IProduct[];
}
