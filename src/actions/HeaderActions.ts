import { Action } from 'redux';

export enum HeaderActions {
    COLLAPSE = "COLLAPSE",
    ACTIVE = "ACTIVE",
    UPDATE_TOTAL = "UPDATE_TOTAL"
}

export interface ICollapseAction extends Action {
    payload: boolean;
}


export interface IActiveAction extends Action {
    payload: string;
}

export interface IUpdateTotalAction extends Action {
    payload: number;
}
