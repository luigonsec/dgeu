import { Action } from 'redux';

export enum MainActions {
    UPDATE_TOTAL = "UPDATE_TOTAL"
}

export interface IUpdateTotalAction extends Action {
    payload: number;
}
