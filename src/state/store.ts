import { Action, createStore } from 'redux';
import IGlobalState, { initialState } from './globalState';
import { HeaderActions, ICollapseAction, IActiveAction, IUpdateTotalAction } from '../actions/HeaderActions';
import { IUpdateOrderAction } from '../actions/MoreProductsActions';
import { MoreProductsActions } from 'src/actions/MoreProductsActions';
import { InventoryActions, IUpdateInventoryAction } from 'src/actions/InventoryActions';

const reducer = (state: IGlobalState = initialState, action: Action) => {
    switch (action.type) {
      case HeaderActions.COLLAPSE:
        const collapseAction = action as ICollapseAction;
        return {...state, collapsed: collapseAction.payload}
      case HeaderActions.ACTIVE:
        const activeAction = action as IActiveAction;
        return {...state, active: activeAction.payload}

        case HeaderActions.UPDATE_TOTAL:
        const updateTotalAction = action as IUpdateTotalAction;
        return {...state, total: updateTotalAction.payload}

        case MoreProductsActions.UPDATE_ORDER:
        const updateOrderAction = action as IUpdateOrderAction;
        return {...state, order: updateOrderAction.payload}

        case InventoryActions.UPDATE_INVENTORY:
        const updateInventoryAction = action as IUpdateInventoryAction;
        return {...state, inventory: updateInventoryAction.payload}

      }

    return state;
  }

  export const store = createStore(reducer, initialState);