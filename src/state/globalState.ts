import data from '../source/order.json'
import inventory from '../source/inventory.json'

import IProduct from '../interfaces/IProduct';


interface IGlobalState {
    collapsed: boolean;
    active: string;
    inventory: IProduct[];
    total: number;
    order: IProduct[];
}

export default IGlobalState;

export const initialState: IGlobalState = {
    active: 'ORDERS',
    collapsed: false,
    inventory: inventory.products,
    order: data.products,
    total: data.products.map((p: IProduct) => p.price * (p.amount ? p.amount : 0)).reduce((a: number, b: number) =>  a + b, 0),
}