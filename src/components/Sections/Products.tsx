import * as React from 'react';
import Question from './partials/Question';
import data from '../../source/all.json'

interface IProductsState {
    active: number;
}

class Products extends React.Component<{}, IProductsState> {
    constructor (props: any) {
        super (props);
        this.state = {
            active: 0
        }
    }

    public vote = () => {
        const active = this.state.active + 1;
        this.setState({
            active
        })
    }
    
    public render () {
        return (
            <div className='row'>
                <div className="col-12 App__section">
                    <h3>Ayúdanos a conocerte mejor.</h3>
                </div>
                <div className="col-12 App__section">
                    <b>Valora productos de manera que podamos conocer mejor tus preferencias.</b>
                </div>
                <div className="col-12 App__section">
                    {
                        this.state.active !== data.products.length ? 
                            <Question 
                                product={data.products[this.state.active]}
                                vote={this.vote}
                            /> : 
                            <div> Ya has revisado todos los productos</div>
                    }
                </div>
            </div>
        );
    }
}

export default Products;