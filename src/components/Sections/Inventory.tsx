import * as React from 'react';
import IProduct from 'src/interfaces/IProduct';
import Product from '../../containers/Product';
interface IInventoryProps {
    inventory: IProduct[];
}

class Inventory extends React.Component<IInventoryProps, {}> {
    constructor (props: any) {
        super (props);
    }

    public renderProducts() {
        return this.props.inventory.map((p: IProduct, index: number) => {
            return (
                <div className='col-12 col-sm-6 col-md-4' key={index}>
                    <Product 
                        product={p} 
                        type={'INVENTORY'}
                    />
                </div>
                
            )
        })
    }
    
    public render () {
        return (
            <div className="row">
                <div className="col-12">
                    <div className='row'>
                        <div className="col-12 App__section">
                            <h3>Mantén actualizado el inventario</h3>
                        </div>
                        <div className="col-12 App__section">
                            <b>Cuánto más actualizado esté el inventario, más fácil será para nosotros ofrecerte la lista de la compra óptima.</b>
                        </div>
                    </div>
                    <div className="row App__section">
                        {this.renderProducts()}
                    </div>
                </div>
            </div>
        );
    }
}

export default Inventory;