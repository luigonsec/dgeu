import * as React from 'react';
import data from '../../source/all.json'
import Product from '../../containers/Product'
import IProduct from '../../interfaces/IProduct';

interface IMoreProductProps {
    order: IProduct[]
    dispatchOrder: (pedido: IProduct[]) => any
}

class MoreProducts extends React.Component<IMoreProductProps, {}> {
    constructor (props: any) {
        super (props);
    }

    public increase = (product: IProduct) => {
        return () => {
            const pedido = this.props.order.slice();
            const producto = pedido.find(p => p.id === product.id)

            if (producto) {
                const futureAmount = producto.amount === undefined ? 0 : producto.amount + 1;
                producto.amount = futureAmount
            } else {
                pedido.push(product)
                product.amount = 1
            }
            
            this.props.dispatchOrder(pedido);
        }
    }

    public decrease = (product: IProduct) => {
        return () => {
            const pedido = this.props.order.slice();
            const producto = pedido.find(p => p.id === product.id)
            if (producto) {
                const futureAmount = producto.amount === undefined ? 0 : producto.amount - 1;
                if (futureAmount< 0) {
                    return;
                }
                producto.amount = futureAmount
            } else {
                pedido.push(product)
                product.amount = 0
            }
            this.props.dispatchOrder(pedido);
        }
    }

    public amountInOrder = (product: IProduct) => {
        const temp = this.props.order.find(p => p.id === product.id)
        return temp ? temp.amount : 0;
    }

    public renderProductos () {


        return data.products.sort((a: IProduct, b: IProduct) => {
            if(a.name < b.name) { return -1; }
            if(a.name > b.name) { return 1; }
            return 0;
        }).map((product: IProduct, index: number) => {
            return (
                <div key={index} className='col-12 col-sm-6 col-md-4'>
                    <Product type={'ORDER'} product={product} />
                </div>
            )
        })
    }
    
    public render () {
        return (
            <div className='row'>
                <div className="col-12 App__section">
                    <h3>Aquí tienes todos los productos que existen</h3>
                </div>
                <div className="col-12 App__section">
                    <b>Aquí encontrarás productos de todas las marcas y supermercados. Podrás tener todos los productos que quieras sin necesidad de recorrer la ciudad entera.</b>
                </div>                
                <div className='col-12 App__section'>
                    <div className="row">
                        {this.renderProductos()}
                    </div>
                </div>
            </div>
        );
    }
}

export default MoreProducts;