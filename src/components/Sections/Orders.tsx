import * as React from 'react';
import Product from '../../containers/Product';

interface IProduct {
    id: number;
    name: string;
    measurement: string;
    price: number;
    amount: number;
    img: string;
} 

interface IOrdersProps {
    order: IProduct[],
    dispatchTotal: (total: number) => any
}
interface IOrdersState {
    removes: number[];
}

class Orders extends React.Component<IOrdersProps, IOrdersState> {
    constructor (props: any) {
        super (props);
        this.state = {
            removes: []
        }
    }

    public renderDirecciones () {
        const opciones = [
            'Calle La Niña 47, Ciudad Expo, Mairena del Aljarafe',
            'Edificio Arena 3, Avenida de la Innovacion, Sevilla'
        ];

        return opciones.map((opcion, index) => {
            return (
            <option key={index}>
                {opcion}
            </option>)
        })
    }

    public renderOpciones () {
        const opciones = ['10-10-2018 17:00',
        '10-10-2018 21:00',
        '11-10-2018 17:00',
        '11-10-2018 18:00',
        '11-10-2018 21:00'];

        return opciones.map((opcion, index) => {
            return (
            <option key={index}>
                {opcion}
            </option>)
        })
    }
    public renderProducts () {
        return this.props.order.filter((p) => p.amount).map((producto: IProduct, index: number) => {
            return (
                <div className="col-12 col-sm-6 col-md-4" key={index}>
                    <Product type={'ORDER'} product={producto} />
                </div>
            )
        })
    }
    
    public render () {

        return (
            <div className='row'>
                <div className="col-12 App__section">
                    <h3>Te hemos preparado la lista de la compra.</h3>
                </div>
                <div className="col-12 App__section">
                    <b>Entrega prevista para:</b>
                    <select name="" id="" className='form-control form-control-sm'>
                        {this.renderOpciones()}
                    </select>                    
                </div>
                <div className="col-12 App__section">
                    <b>Dirección de entrega:</b>
                    <select name="" id="" className='form-control form-control-sm'>
                        {this.renderDirecciones()}
                    </select>                    
                </div>                
                <div className="col-12 App__section">
                    <b>Tu lista:</b>
                    <div className='row'>
                        {this.renderProducts()}
                    </div>
                </div>
            </div>
        );
    }
}

export default Orders;