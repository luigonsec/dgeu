import * as React from 'react';
import IProduct from 'src/interfaces/IProduct';

interface IProductProps {
    dispatchOrder: (product: IProduct[]) => any;
    dispatchInventory: (product: IProduct[]) => any;

    order: IProduct[];
    inventory: IProduct[];
    product: IProduct;
    type: string;

}


class Product extends React.Component<IProductProps, {}> {
    constructor (props: IProductProps) {
        super (props);
    }

    public increase = (product: IProduct) => {
        return () => {
            const pedido = this.props.type === 'ORDER' ? this.props.order.slice() : this.props.inventory.slice();
            const producto = pedido.find(p => p.id === product.id)

            if (producto) {
                const futureAmount = producto.amount === undefined ? 0 : producto.amount + 1;
                producto.amount = futureAmount
            } else {
                pedido.push(product)
                product.amount = 1
            }
            if (this.props.type === 'ORDER') {
                this.props.dispatchOrder(pedido);
            } else {
                this.props.dispatchInventory(pedido);
            }
            
        }
    }

    public decrease = (product: IProduct) => {
        return () => {
            const pedido = this.props.type === 'ORDER' ? this.props.order.slice() : this.props.inventory.slice();
            const producto = pedido.find(p => p.id === product.id)

            if (producto) {
                const futureAmount = producto.amount === undefined ? 0 : producto.amount - 1;
                if (futureAmount < 0) {
                    return;
                }

                producto.amount = futureAmount

            } else {
                pedido.push(product)
                product.amount = 0
            }
            if (this.props.type === 'ORDER') {
                this.props.dispatchOrder(pedido);
            } else {
                this.props.dispatchInventory(pedido);
            }
            
        }
    }
    public amountInOrder = () => {
        const searchIn = this.props.type === 'ORDER' ? this.props.order : this.props.inventory
        const temp = searchIn.find(p => p.id === this.props.product.id)
        return temp ? temp.amount : 0;
    }


    public render () {
        return(
        
            <div className='App__product_item'>
                <div className="row">
                    <div className="col-12">
                        <div className="title">
                            {this.props.product.name}
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-5">
                        <img src={this.props.product.img} alt=""/>
                    </div>
                    <div className="col-7">
                        <div className="price">
                            {this.props.product.price}€ / {this.props.product.measurement}
                        </div>
                        <div className="selector">
                            <div className="row">
                                <div className="col-4">
                                    <div className='decrease' onClick={this.decrease(this.props.product)}>
                                    -
                                    </div>
                                </div>
                                <div className="col-4">
                                    <div className='value'>
                                    {this.amountInOrder()}
                                    </div>
                                </div>
                                <div className="col-4">
                                    <div className='increase' onClick={this.increase(this.props.product)}>
                                    +
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Product;