import * as React from 'react';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faGrinHearts, faFrown, faGrin } from '@fortawesome/free-solid-svg-icons';

library.add(faGrinHearts, faFrown, faGrin);

interface IQuestionProps {
    product: any;
    vote: () => void
}


class Question extends React.Component<IQuestionProps, {}> {
    constructor (props: IQuestionProps) {
        super (props);
    }

    public voteUp = () => {
        this.props.vote()
    }

    public voteDown = () => {
        this.props.vote()
    }

    public renderInfo () {
        return this.props.product.info.map((p: any, index: number) => {
            return (
                <div key={index} className='list-group-item info'><span className='info-title'>{p.name}:</span> {p.amount} {p.unit}</div>
            )
        })
    }


    public render () {
         return(
            <div className="App__Question">
                <div className='row'>
                    <div className="col-12">
                        <div className="title">
                            {this.props.product.name}
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-5 col-md-2">
                        <img src={this.props.product.img}/>  
                    </div>
                    <div className="col-7 col-md-3">
                        <div className="list-group">
                            <div className="list-group-item info">
                                <span className="info-title">
                                    Valor nutricional
                                </span>
                            </div>
                            {this.renderInfo()}
                        </div>                
                    </div>
                </div>
                <div className="row">
                    <div className="col-12 buttons">
                        <button className={'btn button buttonBad'} onClick={this.voteUp}>
                            <FontAwesomeIcon icon={faFrown} />
                        </button>
                        <button className={'btn button buttonGood'} onClick={this.voteDown}>
                            <FontAwesomeIcon icon={faGrin} />
                        </button>
                        <button className={'btn button buttonLove'} onClick={this.voteDown}>
                            <FontAwesomeIcon icon={faGrinHearts} />
                        </button>
                    </div>
                </div>
            </div>
        );
    }
}

export default Question;