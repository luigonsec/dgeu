import * as React from 'react';
import Products from './Sections/Products';
import Inventory from '../containers/Inventory';
import MoreProducts from '../containers/MoreProducts';
import Orders from '../containers/Orders';


interface IMainProps {
    collapsed: boolean;
    active: string;
}


class Main extends React.Component<IMainProps, {}> {
    constructor (props: IMainProps) {
        super (props);
    }
    
    public renderActive () {
        if (this.props.active === 'PRODUCTS') {
            return (<Products />);
        }
        if (this.props.active === 'INVENTORY') {
            return (<Inventory />);
        }
        if (this.props.active === 'ORDERS') {
            return (<Orders />);
        }
        if (this.props.active === 'ORDERS') {
            return (<Orders />);
        }        
        if (this.props.active === 'MORE_PRODUCTS') {
            return (<MoreProducts />);
        }        

        return (<div>Not found {this.props.active}</div>)

    }
    public render () {
        return (
            <div className={'App__Main' + this.props.collapsed ? ' collapsed ' : ''}>
                {this.renderActive()}
            </div>
        );
    }
}

export default Main;