import * as React from 'react';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars, faShoppingCart, faPlus } from '@fortawesome/free-solid-svg-icons';
import IProduct from 'src/interfaces/IProduct';

library.add(faBars, faShoppingCart, faPlus);

interface IHeaderProps {
    collapsed: boolean;
    active: string;
    total: number;
    order: IProduct[];
    dispatchCollapse: (collapsed: boolean) => any;
    dispatchActive: (active: string) => any;
}

interface IHeaderState {
    searchText: string;
}

class Header extends React.Component<IHeaderProps, IHeaderState> {
    constructor (props: IHeaderProps) {
        super (props);
        this.state = { searchText: ""};
    }


    public onOptionClick = (active: string) => {
        return () => {
            this.props.dispatchActive(active)
            this.props.dispatchCollapse(false)
        }
    }

    public render () {

        const onClick = () => {
            this.props.dispatchCollapse(!!!this.props.collapsed)
        }

        return (
        <div className={"row App__Header " + (this.props.collapsed ? 'collapsed' : '') }>
            <div className={'col-12'}>
                <div className={'row options'}>
                    <div className={"col-12 option"} onClick={this.onOptionClick('PRODUCTS')}>
                    Valorar productos
                    </div>                
                    <div className={"col-12 option"} onClick={this.onOptionClick('ORDERS')}>
                    Pedidos
                    </div>                
                    <div className={"col-12 option"} onClick={this.onOptionClick('INVENTORY')}>
                    Inventario
                    </div>                
                </div>
            </div>
            <div className="col-12">
                <div className={'menu'}>
                    <span className="collapse_button" onClick={onClick}>
                        <FontAwesomeIcon icon={faBars} />
                    </span>
                    <span>
                        {['ORDERS', 'MORE_PRODUCTS'].indexOf(this.props.active) !== -1 ? this.renderOrder() : ''}
                    </span>
                </div>

            </div>                                         
        </div>
        );
    }

    public renderOrder () {
        return (

            <div className="cart">

                { this.props.active === 'ORDERS' ? 
                    <span className='add' onClick={this.onOptionClick('MORE_PRODUCTS')}>
                        <FontAwesomeIcon icon={faPlus} />
                    </span> :
                    <span className='add' onClick={this.onOptionClick('ORDERS')}>
                        <FontAwesomeIcon icon={faShoppingCart} />
                    </span>
                }
                    <span className='cart'>
                        {this.props.order.map((a: IProduct) => a.price * (a.amount ? a.amount : 0)).reduce((a: number, b: number) => a + b).toFixed(2)} €
                    </span>
                    
            </div>
        )
    }
}

export default Header;